# Digitale Selbstbestimmung, Privatsphäre und Sicherheit – Die Sammlung der Sammlungen

Dieses Wiki soll eine Übersicht über einige Seiten geben, die Dir bei der Ausübung Deiner digitalen Selbstbestimmung helfen.

## Nutzung

Gehe auf [`fkohrt.gitlab.io/digitale-selbstbestimmung/`](https://fkohrt.gitlab.io/digitale-selbstbestimmung/), um das Wiki anzusehen.

## Selber erzeugen

Falls Du das Wiki auf Deinem eigenen Computer erzeugen willst, findest Du hier die vorbereitenden Schritte. Der Rest geschieht im [`Makefile`](Makefile) durch Aufruf von `make`.

- benötigte Programme: [`git`](https://git-scm.com/), [`npm`](https://npm.community/) (über [Node.js](https://nodejs.org/)), `date` und `make`.
- `git clone https://gitlab.com/fkohrt/digitale-selbstbestimmung`
- `cd digitale-selbstbestimmung/`
- `npm install -g tiddlywiki`
- `make`
- das Wiki liegt nun in `public/index.html`

## Mithelfen

Falls Du mithelfen möchtest, findest Du alle wichtigen Infos dafür in der Datei [CONTRIBUTING.md](CONTRIBUTING.md).

## Lizenz

„Digitale Selbstbestimmung, Privatsphäre und Sicherheit – Die Sammlung der Sammlungen“ von Lukas Schmid, Lea Hinterholzer, Marius Jäger, Florian Kohrt und anderen ist in ihrem Inhalt lizenziert unter [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) und damit gemeinfrei. So weit wie es rechtlich möglich ist, geben die Autor*innen hiermit alle Urheberrechte, Leistungsschutzrechte und verwandten Schutzrechte zusammen mit allen damit verbundenen Ansprüchen und Einreden an dem Inhalt auf.

Die verwendete Software TiddlyWiki ist lizenziert unter der [3-Klausel-BSD-Lizenz](https://tiddlywiki.com/#License). Die Rechte am übrigen Quelltext (insb. Plugins) liegen bei den jeweiligen Autor*innen bzw. sind ebenfalls lizenziert unter CC0 1.0, sofern sie für dieses Wiki entwickelt wurden.

Name|Autor*in|Lizenz
--|--|--
[Minimenu](https://fkohrt.gitlab.io/tiddlywiki-stuff/#%24%3A%2Fplugins%2FTWaddle%2Fminimenu)|Matias Goldman|MIT
[BottomTabs](http://bottomtabs.tiddlyspot.com/)|Alberto Molina Pérez|MIT (Font Awesome: OFL-1.1, MIT)
[EditButtons](https://tid.li/tw5/plugins.html#%24%3A%2Fplugins%2Ftelmiger%2FEditButtons)|Thomas Elmiger|MIT
[tw-base](https://gitlab.com/fkohrt/tw-base)|Florian Kohrt|CC0-1.0 (enthält viele Plugins unter anderen [Lizenzen](https://gitlab.com/fkohrt/tw-base#license))
