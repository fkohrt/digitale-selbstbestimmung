WIKI_DIR := selbstbestimmung
BUILD_DIR := public

CONF_SRC = $(wildcard $(WIKI_DIR)/config/*)
CONF = $(CONF_SRC:$(WIKI_DIR)/config/%=$(WIKI_DIR)/tiddlers/%)
CONF_QUOTED = $(patsubst $(WIKI_DIR)/config/%,'$(WIKI_DIR)/tiddlers/%',$(CONF_SRC))

ifdef COMMIT
COMMIT_ADD := ($(COMMIT))
endif

.PHONY: all base clean $(WIKI_DIR)/tiddlers/LastEdit.tid

all: $(BUILD_DIR)/index.html

base:
	$(MAKE) -C tw-base/

clean:
	rm $(BUILD_DIR)/index.html
	rm $(CONF_QUOTED)
	rm $(WIKI_DIR)/tiddlers/LastEdit.tid
	rm temp/bottomtabs.html
	rm $(WIKI_DIR)/tiddlers/$$__bottomtabs-common.json
	rm $(WIKI_DIR)/tiddlers/$$__bottomtabs-reading.json
	rm $(WIKI_DIR)/tiddlers/$$__edit-buttons.json
	rm temp/telmiger-plugins.html
	$(WIKI_DIR)/tiddlers/$$__font-awesome.json
	$(MAKE) -C tw-base/ clean

$(BUILD_DIR)/index.html: $(BUILD_DIR)/ $(CONF) $(WIKI_DIR)/tiddlers/LastEdit.tid $(WIKI_DIR)/tiddlers/$$__bottomtabs-common.json $(WIKI_DIR)/tiddlers/$$__bottomtabs-reading.json $(WIKI_DIR)/tiddlers/$$__edit-buttons.json base
	tiddlywiki $(WIKI_DIR)/ --output $(BUILD_DIR)/ --build index

$(BUILD_DIR)/:
	mkdir $@

$(CONF):
	cp '$(WIKI_DIR)/config/$(@F)' '$@'

$(WIKI_DIR)/tiddlers/LastEdit.tid:
	echo 'title: $$:/fk/inventory/LastEdit' > $@
	env TZ=Europe/Berlin date +"text: Stand dieser Version: %Y-%m-%d, %H:%M Uhr $(COMMIT_ADD)" >> $@

$(WIKI_DIR)/tiddlers/$$__bottomtabs-common.json: temp/bottomtabs.html $(WIKI_DIR)/tiddlers/$$__font-awesome.json
	tiddlywiki --load $< --output $(WIKI_DIR)/tiddlers/ --render $$:/plugins/amp/BT/common '$(@F)' text/plain $$:/core/templates/json-tiddler

$(WIKI_DIR)/tiddlers/$$__bottomtabs-reading.json: temp/bottomtabs.html $(WIKI_DIR)/tiddlers/$$__font-awesome.json
	tiddlywiki --load $< --output $(WIKI_DIR)/tiddlers/ --render $$:/plugins/amp/BT/reading '$(@F)' text/plain $$:/core/templates/json-tiddler

$(WIKI_DIR)/tiddlers/$$__font-awesome.json: temp/bottomtabs.html
	tiddlywiki --load $< --output $(WIKI_DIR)/tiddlers/ --render $$:/plugins/TheDiveO/FontAwesome/fonts/FontAwesome '$(@F)' text/plain $$:/core/templates/json-tiddler

temp/bottomtabs.html: temp/
	wget -O $@ http://bottomtabs.tiddlyspot.com/

$(WIKI_DIR)/tiddlers/$$__edit-buttons.json: temp/telmiger-plugins.html
	tiddlywiki --load $< --output $(WIKI_DIR)/tiddlers/ --render $$:/plugins/telmiger/EditButtons '$(@F)' text/plain $$:/core/templates/json-tiddler

temp/telmiger-plugins.html: temp/
	wget -O $@ https://tid.li/tw5/plugins.html

temp/:
	mkdir temp/

