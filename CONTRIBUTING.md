# Richtlinien zum Mitmachen

Hallo und danke, dass Du mithelfen möchtest! Im Folgenden ein paar Infos, die Dir dabei helfen werden.

Du musst nicht programmieren können, um dieses Projekt zu unterstützen. Auch das Melden eines Fehlers oder das Vorschlagen von Verbesserungen bringt das Projekt voran. Du benötigst dafür ein GitLab-Konto.

## Einen Fehler melden oder eine Verbesserung vorschlagen

Wenn Du einen Fehler findest oder eine Idee hast, wie man das Projekt verbessern kann, sind die sog. [Issues](https://gitlab.com/fkohrt/digitale-selbstbestimmung/-/issues) der richtige Ort zum Diskutieren. Schau einmal nach, ob es schon ein Issue zum Thema gibt, ansonsten [erstelle ein Neues](https://opensource.guide/de/how-to-contribute/#ein-issue-erstellen).

## Änderungen am Projekt vornehmen

Wenn Du eine Änderung zu diesem Projekt beiträgst, gehen wir davon aus, dass Folgendes auf Dich zutrifft:

- Du bist Dir über die Lizenz dieses Projektes, CC0 1.0, im Klaren.
- Du hast die [Regelungen dieser Lizenz](COPYING.md) gelesen und den gewollten rechtlichen Effekt dieses Werkzeugs verstanden. Creative Commons stellt eine [allgemeinverständliche Zusammenfassung](https://creativecommons.org/publicdomain/zero/1.0/deed.de) der Lizenz zur Verfügung.
- Du bestätigst, dass Deine Änderung ebenfalls unter dieser Lizenz weiterverbreitet werden darf. So weit wie es rechtlich möglich ist, gibst Du damit alle Urheberrechte, Leistungsschutzrechte und verwandten Schutzrechte zusammen mit allen damit verbundenen Ansprüchen und Einreden an Deiner Änderung auf.

Dieses Projekt nutzt die Software Git zur Versionsverwaltung, die das Erstellen eines _Forks_ und eines anschließenden _Pull- bzw. Merge-Requests_ erfordert. Falls Du damit noch nicht vertraut bist, findest Du unter [opensource.guide](https://opensource.guide/de/how-to-contribute/#ein-pull-request-erstellen) eine kurze Einführung. Beachte dabei: Der zentrale Branch in diesem Repository heißt `main`, nicht `master`.

### Kleine Änderungen

Kleine Änderungen, z. B. die Beseitigung von Tippfehlern, kannst Du vornehmen, indem Du auf der Wiki-Seite [`fkohrt.gitlab.io/digitale-selbstbestimmung/`](https://fkohrt.gitlab.io/digitale-selbstbestimmung/) oben rechts auf das `V` klickst, um in den Admin-Modus zu gelangen. Nun kannst Du für einzelne Beiträge links neben dem `X` auf das Symbol zum Bearbeiten – einen Stift – klicken. In der daraufhin geöffneten Ansicht ist neben einem Stern die Frage zu lesen: „Kannst Du diesen Beitrag verbessern?“ Klicke auf den Link „diesen Beitrag auf GitLab bearbeiten“, der sich rechts davon befindet, um zum GitLab-Projekt geleitet zu werden. In der nun offenen Seite kannst Du auf die blaue Schaltfläche `Edit` klicken. Du musst Dich ggf. erst anmelden, bevor Du anschließend auf die grüne Schaltfläche `Fork` klicken kannst. Nach kurzer Wartezeit solltest Du nun die Möglichkeit haben, Änderungen am Beitrag vorzunehmen. Wenn Du fertig bist, kannst Du Deine Änderungen im Feld `Commit message` kurz beschreiben und anschließend auf die grüne Schaltfläche `Commit changes` klicken. Auf der sich nun öffnenden Seite musst Du noch auf die grüne Schaltfläche `Submit merge request` klicken. Fertig! Wir sehen uns Deine Änderung an und übernehmen sie gegebenenfalls.

### Größere Änderungen

Für alle [nicht-trivialen Änderungen](https://www.gnu.org/prep/maintain/maintain.html#Legally-Significant) bitten wir Dich, das folgende _Developer Certificate of Origin_ zu bestätigen, indem Du Deine Commits mit der Option `--signoff` durchführst.

```
Developer Certificate of Origin
Version 1.1

Copyright (C) 2004, 2006 The Linux Foundation and its contributors.
1 Letterman Drive
Suite D4700
San Francisco, CA, 94129

Everyone is permitted to copy and distribute verbatim copies of this
license document, but changing it is not allowed.


Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I
    have the right to submit it under the open source license
    indicated in the file; or

(b) The contribution is based upon previous work that, to the best
    of my knowledge, is covered under an appropriate open source
    license and I have the right under that license to submit that
    work with modifications, whether created in whole or in part
    by me, under the same open source license (unless I am
    permitted to submit under a different license), as indicated
    in the file; or

(c) The contribution was provided directly to me by some other
    person who certified (a), (b) or (c) and I have not modified
    it.

(d) I understand and agree that this project and the contribution
    are public and that a record of the contribution (including all
    personal information I submit with it, including my sign-off) is
    maintained indefinitely and may be redistributed consistent with
    this project or the open source license(s) involved.
```

Damit Du schnell einen Eintrag ändern oder hinzufügen kannst, erklären wir im Folgenden, wie das geht: Für jeden Wiki-Eintrag liegt eine Datei mit der Endung `.tid` im Ordner `selbstbestimmung/tiddlers/` mit dem folgenden Format:

```
title: TITEL
url: URL
description: BESCHREIBUNG
tags: INHALTSVERZEICHNIS-SCHLAGWORT SCHLAGWORT [[SCHLAGWORT MIT LEERZEICHEN]]

{{||$:/fk/inventory/EntryTemplate}}
```

- `TITEL` ist der Name des Eintrags
- `URL` ist die Internetadresse dieses Eintrags inklusive Protokoll (z. B. `https://example.org/`)
- `BESCHREIBUNG` ist ein kurzer Aufmacher, der Ziel und Inhalt des Eintrags beschreibt
- auf `tags: ` folgt eine Reihe durch Leerzeichen getrennter Schlagwörter, wobei mindestens das erste davon auch aus dem Inhaltsverzeichnis stammen sollte; falls das Schlagwort Leerzeichen enthält, muss es von doppelten eckigen Klammern umgeben werden
- `TITEL`, `URL`, `BESCHREIBUNG` und die Schlagwörter dürfen keine Zeilenumbrüche enthalten
- es folgt eine Leerzeile, alles Folgende sollte im Normalfall wie in der Formatangabe übernommen werden

Inhaltsverzeichnis-Tags werden im gleichen Ordner in der Datei `tags-with-existing-tiddler.json` verwaltet.

Wenn Du bestehende Einträge bearbeiten willst, suche die Datei mit dem entsprechenden Link und bearbeite sie. Wenn Du einen neuen Eintrag hinzufügen willst, füge eine Datei nach obigem Format mit der Endung `.tid` und passendem Namen hinzu. Für die Darstellung im Wiki ist der Dateiname bis auf die Endung egal.

Wenn Du Deinen Text besonders auszeichnen willst, etwa _kursiv_ oder **fett**, wirf einen Blick in die offizielle Beschreibung der [WikiText](https://tiddlywiki.com/#WikiText)-Syntax.

## Software Dritter hinzufügen

Software Dritter kann in dem Projekt verwendet werden, so lange sie unter einer [GPL-kompatiblen Lizenz](https://www.gnu.org/licenses/license-list.html#GPLCompatibleLicenses) veröffentlicht wurde. Um späteres Aktualisieren zu erleichtern ist es ratsam, die Software in einheitlicher Weise während der Erzeugung des Wikis einzubinden – entweder als sog. _git submodule_ oder über das [`Makefile`](Makefile). Ist der Quelltext in einem Git-Repository veröffentlicht, kann man dieses dem Projekt über folgenden Befehl (in diesem Beispiel von GitLab.com) als _submodule_ hinzufügen (der Ordner `modules` muss erstellt werden, falls er noch nicht vorhanden ist): `git submodule add --name NAME https://gitlab.com/BENUTZER/NAME modules/NAME`. Falls es sich um ein TiddlyWiki-Plugin handelt, kann dieses dann durch einen symbolischen Link geladen werden: `ln -s ../../modules/NAME/PFAD_ZUM_PLUGIN/ selbstbestimmung/plugins/`. Falls das Plugin online in einem TiddlyWiki enthalten ist, kann das Wiki auch im `Makefile` heruntergeladen werden, um anschließend das entsprechende Plugin zu extrahieren – im Makefile `tw-base/Makefile` finden sich einige Beispiele dafür.

